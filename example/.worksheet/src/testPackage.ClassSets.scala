package testPackage

object ClassSets {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(80); 
  val t1 = new NonEmpty(3, Empty, Empty);System.out.println("""t1  : testPackage.NonEmpty = """ + $show(t1 ));$skip(24); 
  val t2 = t1 include 4;System.out.println("""t2  : testPackage.IntSet = """ + $show(t2 ));$skip(41); 
  val t3 = new NonEmpty(7, Empty, Empty);System.out.println("""t3  : testPackage.NonEmpty = """ + $show(t3 ));$skip(23); 
	val t4 = t3 include 4;System.out.println("""t4  : testPackage.IntSet = """ + $show(t4 ));$skip(23); 
	val t5 = t4 include 5;System.out.println("""t5  : testPackage.IntSet = """ + $show(t5 ));$skip(24); 
	val t6 = t5 include 12;System.out.println("""t6  : testPackage.IntSet = """ + $show(t6 ));$skip(23); 
	val t7 = t6 include 9;System.out.println("""t7  : testPackage.IntSet = """ + $show(t7 ));$skip(22); 
	val t8 = t7 union t2;System.out.println("""t8  : testPackage.IntSet = """ + $show(t8 ))}
}

abstract class IntSet {
	def include(x: Int): IntSet
	def contains(x: Int): Boolean
	def union(other: IntSet): IntSet
}

//class Empty extends IntSet {
//	def contains(x: Int): Boolean = false
//	def include(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
//	def union(other: IntSet): IntSet = other
//	override def toString = "."
//}

object Empty extends IntSet {
	def contains(x: Int): Boolean = false
	def include(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
	def union(other: IntSet): IntSet = other
	override def toString = "."
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
	def contains(x: Int): Boolean =
		if (x < elem) left contains x
		else if (x > elem) right contains x
		else true
		
	def include(x: Int): IntSet =
		if (x < elem) new NonEmpty(elem, left include x, right)
		else if (x > elem) new NonEmpty(elem, left, right include x)
		else this

	def union(other: IntSet): IntSet =
	 	((left union right) union other) include elem

	override def toString = "{" + left + elem + right + "}"
}
