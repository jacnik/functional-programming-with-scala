package testPackage

object testWS {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(220); 
    def sum(f: Int => Int, a: Int, b: Int): Int = {
      def loop(a: Int, acc: Int): Int = {
        if (a > b) acc
        else loop(a + 1, f(a) + acc)
      }
      loop(a, 0)
   };System.out.println("""sum: (f: Int => Int, a: Int, b: Int)Int""");$skip(24); val res$0 = 
   sum(x => x*x, 3 , 5);System.out.println("""res0: Int = """ + $show(res$0));$skip(112); 
   
   def sum2(f: Int => Int)(a:Int, b:Int): Int = {
   		if(a > b) 0
   		else f(a) + sum2(f)( a + 1, b)
   };System.out.println("""sum2: (f: Int => Int)(a: Int, b: Int)Int""");$skip(25); val res$1 = 
   sum2(x => x*x)(3 , 5);System.out.println("""res1: Int = """ + $show(res$1));$skip(171); 
   
   def product(f: Int => Int): (Int, Int) => Int = {
   		def func(a: Int, b: Int): Int = {
   			if( a > b ) 1
   			else f(a) * func(a + 1, b)
   		}
   		func
   };System.out.println("""product: (f: Int => Int)(Int, Int) => Int""");$skip(121); 

   def product2(f: Int => Int)(a: Int,b: Int): Int = {
   			if( a > b ) 1
   			else f(a) * product2(f)(a + 1, b)
   };System.out.println("""product2: (f: Int => Int)(a: Int, b: Int)Int""");$skip(33); val res$2 = 
      
   product2(x => x)(1, 4);System.out.println("""res2: Int = """ + $show(res$2));$skip(29); val res$3 = 
   product2(x => x* x)(3, 5);System.out.println("""res3: Int = """ + $show(res$3));$skip(66); 
   
   def fact(n: Int): Int = {
   		product2(x => x)(1, n)
   };System.out.println("""fact: (n: Int)Int""");$skip(15); val res$4 = 
   
   fact(4);System.out.println("""res4: Int = """ + $show(res$4));$skip(188); 
   
   def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int,b: Int): Int = {
   		if (a > b) unit
   		else combine(f(a), mapReduce(f, combine, unit)(a + 1, b))
   };System.out.println("""mapReduce: (f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int, b: Int)Int""");$skip(118); 
   
   def productRed(f: Int => Int)(a: Int,b: Int): Int = {
   		mapReduce(f, (x: Int, y: Int) => x*y, 1)(a, b)
   };System.out.println("""productRed: (f: Int => Int)(a: Int, b: Int)Int""");$skip(35); val res$5 = 
   
   productRed(x => x* x)(3, 5);System.out.println("""res5: Int = """ + $show(res$5));$skip(31); val res$6 = 
   productRed(x => x* x)(3, 4);System.out.println("""res6: Int = """ + $show(res$6))}
}
