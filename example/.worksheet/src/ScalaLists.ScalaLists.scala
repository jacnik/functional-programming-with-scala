package ScalaLists

object ScalaLists {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(83); 
  println("Welcome to the Scala worksheet");$skip(57); 
   
  val l = new Cons(1, new Cons(2, new Cons(3, Nil)));System.out.println("""l  : ScalaLists.Cons[Int] = """ + $show(l ));$skip(180); 
  def nth[T](n: Int, list: List[T]): T = {
  	if (list.isEmpty) throw new IndexOutOfBoundsException("Empty List")
  	else if(n == 0) list.head
  	else nth[T](n - 1, list.tail)
  };System.out.println("""nth: [T](n: Int, list: ScalaLists.List[T])T""");$skip(12); val res$0 = 

	nth(2, l);System.out.println("""res0: Int = """ + $show(res$0))}
	
	//def f(xs: List[NonEmpty], x: Empty) = xs prepend x
}
