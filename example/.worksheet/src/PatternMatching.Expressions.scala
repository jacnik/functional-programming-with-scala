package PatternMatching

object Expressions {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(78); val res$0 = 
  Sum(Number(1), Number(2)).eval;System.out.println("""res0: Int = """ + $show(res$0));$skip(48); val res$1 = 
  Sum(Number(1), Sum(Number(2),Number(3))).eval;System.out.println("""res1: Int = """ + $show(res$1));$skip(35); val res$2 = 


  Sum(Number(1), Number(2)).show;System.out.println("""res2: String = """ + $show(res$2));$skip(48); val res$3 = 
  Sum(Number(1), Sum(Number(2),Number(3))).show;System.out.println("""res3: String = """ + $show(res$3));$skip(400); 
  
  
  def superShow(e: Expr): String = e match {
    case Number(n) => n.toString
    case Sum(e1, e2) => superShow(e1) + " + " + superShow(e2)
  	case Var(v) => v
  	case Prod(e1, e2) =>
  		(e1 match {
				case Sum(_,_) => "(" + superShow(e1) + ")"
				case _ => superShow(e1)
  		}) + " * " +
  		(e2 match {
				case Sum(_,_) => "(" + superShow(e2) + ")"
				case _ => superShow(e2)
  		})
  };System.out.println("""superShow: (e: PatternMatching.Expr)String""");$skip(60); val res$4 = 
  
  
  superShow(Sum(Number(1), Sum(Number(2),Number(3))));System.out.println("""res4: String = """ + $show(res$4));$skip(54); val res$5 = 
  superShow(Sum(Prod(Number(2), Var("x")), Var("y")));System.out.println("""res5: String = """ + $show(res$5));$skip(60); val res$6 = 
  
  
  superShow(Prod(Sum(Number(2), Var("x")), Var("y")));System.out.println("""res6: String = """ + $show(res$6))}
}
