package testPackage

object Rational {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(67); 
  val x = new Rational(1, 2);System.out.println("""x  : testPackage.Rational = """ + $show(x ));$skip(8); val res$0 = 
  x.num;System.out.println("""res0: Int = """ + $show(res$0));$skip(8); val res$1 = 
  x.den;System.out.println("""res1: Int = """ + $show(res$1));$skip(32); 
  
  val y = new Rational(2, 3);System.out.println("""y  : testPackage.Rational = """ + $show(y ));$skip(11); val res$2 = 
  x.add(y);System.out.println("""res2: testPackage.Rational = """ + $show(res$2));$skip(8); val res$3 = 
  x.neg;System.out.println("""res3: testPackage.Rational = """ + $show(res$3));$skip(32); 
  
  val a = new Rational(1, 3);System.out.println("""a  : testPackage.Rational = """ + $show(a ));$skip(29); 
  val b = new Rational(5, 7);System.out.println("""b  : testPackage.Rational = """ + $show(b ));$skip(29); 
  val c = new Rational(3, 2);System.out.println("""c  : testPackage.Rational = """ + $show(c ));$skip(21); val res$4 = 
  
  a.sub(b).sub(c);System.out.println("""res4: testPackage.Rational = """ + $show(res$4));$skip(20); val res$5 = 
  a.sub2(b).sub2(c);System.out.println("""res5: testPackage.Rational = """ + $show(res$5));$skip(11); val res$6 = 
  b.add(b);System.out.println("""res6: testPackage.Rational = """ + $show(res$6));$skip(15); val res$7 = 
  
  a.less(b);System.out.println("""res7: Boolean = """ + $show(res$7));$skip(11); val res$8 = 
  a.max(b);System.out.println("""res8: testPackage.Rational = """ + $show(res$8));$skip(21); val res$9 = 
  
  new Rational(2);System.out.println("""res9: testPackage.Rational = """ + $show(res$9))}
}


class Rational(x: Int, y: Int) {
	require( y != 0, "denominator must be non-zero")
	
	def this(x: Int) = this(x, 1)
	
	def num = x
	def den = y
	
	def add(other: Rational) =
		new Rational(
			this.num * other.den + other.num * this.den,
			this.den * other.den)
	
	def sub(other: Rational) =
		new Rational(
			this.num * other.den - other.num * this.den,
			this.den * other.den)

	def sub2(other: Rational) = this.add(other.neg)
	
	def neg: Rational =
		if (den < 0) new Rational(num, -1 * den)
		else new Rational(-num, den)
	
	def less(other: Rational) = this.num * other.den < this.den * other.num
	
	def max(other: Rational) = if(this.less(other)) other else this
	
	override def toString: String = num / g + "/" + den / g
	
	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	private val g = gcd(x, y)
}
