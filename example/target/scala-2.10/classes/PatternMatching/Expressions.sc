package PatternMatching

object Expressions {
  Sum(Number(1), Number(2)).eval                  //> res0: Int = 3
  Sum(Number(1), Sum(Number(2),Number(3))).eval   //> res1: Int = 6


  Sum(Number(1), Number(2)).show                  //> res2: String = 1 + 2
  Sum(Number(1), Sum(Number(2),Number(3))).show   //> res3: String = 1 + 2 + 3
  
  
  def superShow(e: Expr): String = e match {
    case Number(n) => n.toString
    case Sum(e1, e2) => superShow(e1) + " + " + superShow(e2)
  	case Var(v) => v
  	case Prod(e1, e2) =>
  		(e1 match {
				case Sum(_,_) => "(" + superShow(e1) + ")"
				case _ => superShow(e1)
  		}) + " * " +
  		(e2 match {
				case Sum(_,_) => "(" + superShow(e2) + ")"
				case _ => superShow(e2)
  		})
  }                                               //> superShow: (e: PatternMatching.Expr)String
  
  
  superShow(Sum(Number(1), Sum(Number(2),Number(3))))
                                                  //> res4: String = 1 + 2 + 3
  superShow(Sum(Prod(Number(2), Var("x")), Var("y")))
                                                  //> res5: String = 2 * x + y
  
  
  superShow(Prod(Sum(Number(2), Var("x")), Var("y")))
                                                  //> res6: String = (2 + x) * y
}