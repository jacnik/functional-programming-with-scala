package ScalaLists

object ScalaLists {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
   
  val l = new Cons(1, new Cons(2, new Cons(3, Nil)))
                                                  //> l  : ScalaLists.Cons[Int] = ScalaLists.Cons@54ed21d3
  def nth[T](n: Int, list: List[T]): T = {
  	if (list.isEmpty) throw new IndexOutOfBoundsException("Empty List")
  	else if(n == 0) list.head
  	else nth[T](n - 1, list.tail)
  }                                               //> nth: [T](n: Int, list: ScalaLists.List[T])T

	nth(2, l)                                 //> res0: Int = 3
	
	//def f(xs: List[NonEmpty], x: Empty) = xs prepend x
}