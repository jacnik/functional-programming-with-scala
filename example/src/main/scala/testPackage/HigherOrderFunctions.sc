package testPackage

object HigherOrderFunctions {
      def sum(f: Int => Int, a: Int, b: Int): Int = {
      def loop(a: Int, acc: Int): Int = {
        if (a > b) acc
        else loop(a + 1, f(a) + acc)
      }
      loop(a, 0)
   }                                              //> sum: (f: Int => Int, a: Int, b: Int)Int
   sum(x => x*x, 3 , 5)                           //> res0: Int = 50
   
   def sum2(f: Int => Int)(a:Int, b:Int): Int = {
   		if(a > b) 0
   		else f(a) + sum2(f)( a + 1, b)
   }                                              //> sum2: (f: Int => Int)(a: Int, b: Int)Int
   sum2(x => x*x)(3 , 5)                          //> res1: Int = 50
   
   def product(f: Int => Int): (Int, Int) => Int = {
   		def func(a: Int, b: Int): Int = {
   			if( a > b ) 1
   			else f(a) * func(a + 1, b)
   		}
   		func
   }                                              //> product: (f: Int => Int)(Int, Int) => Int

   def product2(f: Int => Int)(a: Int,b: Int): Int = {
   			if( a > b ) 1
   			else f(a) * product2(f)(a + 1, b)
   }                                              //> product2: (f: Int => Int)(a: Int, b: Int)Int
      
   product2(x => x)(1, 4)                         //> res2: Int = 24
   product2(x => x* x)(3, 5)                      //> res3: Int = 3600
   
   def fact(n: Int): Int = {
   		product2(x => x)(1, n)
   }                                              //> fact: (n: Int)Int
   
   fact(4)                                        //> res4: Int = 24
   
   def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int,b: Int): Int = {
   		if (a > b) unit
   		else combine(f(a), mapReduce(f, combine, unit)(a + 1, b))
   }                                              //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int, b
                                                  //| : Int)Int
   
   def productRed(f: Int => Int)(a: Int,b: Int): Int = {
   		mapReduce(f, (x: Int, y: Int) => x*y, 1)(a, b)
   }                                              //> productRed: (f: Int => Int)(a: Int, b: Int)Int
   
   productRed(x => x* x)(3, 5)                    //> res5: Int = 3600
   productRed(x => x* x)(3, 4)                    //> res6: Int = 144
}