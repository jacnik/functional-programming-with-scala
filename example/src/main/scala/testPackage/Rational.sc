package testPackage

object Rational {
  val x = new Rational(1, 2)                      //> x  : testPackage.Rational = 1/2
  x.num                                           //> res0: Int = 1
  x.den                                           //> res1: Int = 2
  
  val y = new Rational(2, 3)                      //> y  : testPackage.Rational = 2/3
  x.add(y)                                        //> res2: testPackage.Rational = 7/6
  x.neg                                           //> res3: testPackage.Rational = 1/-2
  
  val a = new Rational(1, 3)                      //> a  : testPackage.Rational = 1/3
  val b = new Rational(5, 7)                      //> b  : testPackage.Rational = 5/7
  val c = new Rational(3, 2)                      //> c  : testPackage.Rational = 3/2
  
  a.sub(b).sub(c)                                 //> res4: testPackage.Rational = -79/42
  a.sub2(b).sub2(c)                               //> res5: testPackage.Rational = -79/42
  b.add(b)                                        //> res6: testPackage.Rational = 10/7
  
  a.less(b)                                       //> res7: Boolean = true
  a.max(b)                                        //> res8: testPackage.Rational = 5/7
  
  new Rational(2)                                 //> res9: testPackage.Rational = 2/1
}


class Rational(x: Int, y: Int) {
	require( y != 0, "denominator must be non-zero")
	
	def this(x: Int) = this(x, 1)
	
	def num = x
	def den = y
	
	def add(other: Rational) =
		new Rational(
			this.num * other.den + other.num * this.den,
			this.den * other.den)
	
	def sub(other: Rational) =
		new Rational(
			this.num * other.den - other.num * this.den,
			this.den * other.den)

	def sub2(other: Rational) = this.add(other.neg)
	
	def neg: Rational =
		if (den < 0) new Rational(num, -1 * den)
		else new Rational(-num, den)
	
	def less(other: Rational) = this.num * other.den < this.den * other.num
	
	def max(other: Rational) = if(this.less(other)) other else this
	
	override def toString: String = num / g + "/" + den / g
	
	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	private val g = gcd(x, y)
}