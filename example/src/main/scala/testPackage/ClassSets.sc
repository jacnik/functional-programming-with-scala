package testPackage

object ClassSets {
  val t1 = new NonEmpty(3, Empty, Empty)          //> t1  : testPackage.NonEmpty = {.3.}
  val t2 = t1 include 4                           //> t2  : testPackage.IntSet = {.3{.4.}}
  val t3 = new NonEmpty(7, Empty, Empty)          //> t3  : testPackage.NonEmpty = {.7.}
	val t4 = t3 include 4                     //> t4  : testPackage.IntSet = {{.4.}7.}
	val t5 = t4 include 5                     //> t5  : testPackage.IntSet = {{.4{.5.}}7.}
	val t6 = t5 include 12                    //> t6  : testPackage.IntSet = {{.4{.5.}}7{.12.}}
	val t7 = t6 include 9                     //> t7  : testPackage.IntSet = {{.4{.5.}}7{{.9.}12.}}
	val t8 = t7 union t2                      //> t8  : testPackage.IntSet = {.3{.4{{.5{.7.}}9{.12.}}}}
}

abstract class IntSet {
	def include(x: Int): IntSet
	def contains(x: Int): Boolean
	def union(other: IntSet): IntSet
}

//class Empty extends IntSet {
//	def contains(x: Int): Boolean = false
//	def include(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
//	def union(other: IntSet): IntSet = other
//	override def toString = "."
//}

object Empty extends IntSet {
	def contains(x: Int): Boolean = false
	def include(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
	def union(other: IntSet): IntSet = other
	override def toString = "."
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
	def contains(x: Int): Boolean =
		if (x < elem) left contains x
		else if (x > elem) right contains x
		else true
		
	def include(x: Int): IntSet =
		if (x < elem) new NonEmpty(elem, left include x, right)
		else if (x > elem) new NonEmpty(elem, left, right include x)
		else this

	def union(other: IntSet): IntSet =
	 	((left union right) union other) include elem

	override def toString = "{" + left + elem + right + "}"
}