package waterPouring

object pouringTest {
  val problem = new Pouring(Vector(4, 9))         //> problem  : waterPouring.Pouring = waterPouring.Pouring@17a220d7
  problem.moves                                   //> res0: scala.collection.immutable.IndexedSeq[Product with Serializable with w
                                                  //| aterPouring.pouringTest.problem.Move] = Vector(Empty(0), Empty(1), Fill(0), 
                                                  //| Fill(1), Pour(0,1), Pour(1,0))
  //problem.pathSets.take(3).toList
  problem.solutions(6)                            //> res1: Stream[waterPouring.pouringTest.problem.Path] = Stream(Fill(1) Pour(1,
                                                  //| 0) Empty(0) Pour(1,0) Empty(0) Pour(1,0) Fill(1) Pour(1,0)--> Vector(4, 6), 
                                                  //| ?)
}