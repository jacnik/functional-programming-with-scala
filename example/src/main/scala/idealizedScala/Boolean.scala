package idealizedScala
//package idealized.scala


abstract class myBoolean {
	def ifThenElse[T](t: => T, e: => T): T
	
	def && (x: => myBoolean): myBoolean = ifThenElse(x, myFalse)
	def || (x: => myBoolean): myBoolean = ifThenElse(myTrue, x)
	def unary_!(): myBoolean = ifThenElse(myFalse, myTrue)

	def == (x: myBoolean): myBoolean = ifThenElse(x, x.unary_!)
	def != (x: myBoolean): myBoolean = ifThenElse(x.unary_!, x)
	
	def < (x: myBoolean): myBoolean = ifThenElse(myFalse, x)
}

object myTrue extends myBoolean {
  def ifThenElse[T](t: => T, e: => T) = t
}

object myFalse extends myBoolean {
  def ifThenElse[T](t: => T, e: => T) = e
}