package idealizedScala

abstract class Nat {
	def isZero: myBoolean
	def predecessor: Nat
	def successor: Nat = new Succ(this)
	def + (that: Nat): Nat
	def - (that: Nat): Nat
}

object Zero extends Nat {
  def isZero = myTrue
  def predecessor = throw new Error("Zero has no predecessors")
  def + (that: Nat) = that
  def - (that: Nat) = if(that.isZero == myTrue) this else throw new Error("Negative value")
}

class Succ(n: Nat) extends Nat {
  	def isZero = myFalse
	def predecessor = n
	def + (that: Nat) = new Succ(n + that)
	def - (that: Nat) = if(that.isZero == myTrue) this else this.predecessor - that.predecessor // n - that.predecessor
}