import objsets._

object ObjSetsTests {
    val set1 = new Empty                          //> set1  : objsets.Empty = objsets.Empty@207c5965
    val set2 = set1.incl(new Tweet("a", "a body", 30))
                                                  //> set2  : objsets.TweetSet = objsets.NonEmpty@3ef8dcfe
    val set3 = set2.incl(new Tweet("b", "b body", 20))
                                                  //> set3  : objsets.TweetSet = objsets.NonEmpty@35f3198f
    val c = new Tweet("c", "c body", 7)           //> c  : objsets.Tweet = User: c
                                                  //| Text: c body [7]
    val d = new Tweet("d", "d body", 9)           //> d  : objsets.Tweet = User: d
                                                  //| Text: d body [9]
    val set4c = set3.incl(c)                      //> set4c  : objsets.TweetSet = objsets.NonEmpty@6b1316f4
    val set4d = set3.incl(d)                      //> set4d  : objsets.TweetSet = objsets.NonEmpty@32482417
    val set5 = set4c.incl(d)                      //> set5  : objsets.TweetSet = objsets.NonEmpty@1e397bcb
    
    set5.filter(tw => tw.user == "a")             //> res0: objsets.TweetSet = objsets.NonEmpty@72d9ceea
    
    set5.mostRetweeted                            //> res1: objsets.Tweet = User: a
                                                  //| Text: a body [30]
}