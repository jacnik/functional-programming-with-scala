package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
      assert(weight(t2) === 9)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }
  
  // my tests below:
  
//  test("Decoded Secret") {
//	  val secret = decodedSecret
//	  println(secret)
//  }
  
//  	test("singleton") {
//  		assert(!singleton(List()))
//  		assert(singleton(List(createCodeTree("hello".toList))))
//  		assert(!singleton(List(createCodeTree("hello".toList), createCodeTree("world".toList))))
//  	}
//  
//  	test("addToAcc") {
//  		//println(addToAcc('a', List(('d', 1), ('a', 2), ('c', 2)))) 
//  	}
//  	
//  	test("times") {
//  	  assert(times(List('a', 'b', 'a')) === List(('a', 2), ('b', 1)))
//  	  //println( times(List('a', 'b', 'a')) )
//  	}
  	
//  test("makeOrderedLeafList") {
//    var t = makeOrderedLeafList(List(('d', 3), ('a', 1), ('c', 2)))
//    println(t)
//  }
  
  
  	test("Decode") {
  	  var codeTree = createCodeTree("helloalbcedegsdhjkdnskdfdf".toList)
  	  var enc = encode(codeTree)("hello".toList)
  	  var dec = decode(codeTree, enc)
  	  var test = until(singleton, combine)(makeOrderedLeafList(times("hello".toList)))
  	  //println(codeTree)
//  	  println(enc)
//  	  println(dec)
//  	  println(decode(codeTree, enc))
  	}
//  test("encodeAcc2") {
//   var enc = encode(codeTree)("hello".toList)
//   var = encodeAcc2(List(), )
//  }
  	
  	test("codeBits") {
  		var codeTb = ('a', List(0,0,0)) :: ('b', List(0,0,1)) :: ('c', List(0,1,1)) :: ('d', List(1,1,1)) :: Nil
//  		var res = codeBits(codeTb)('d')
//  		println(res)
  		assert(codeBits(codeTb)('a') === List(0,0,0))
  		assert(codeBits(codeTb)('b') === List(0,0,1))
  		assert(codeBits(codeTb)('c') === List(0,1,1))
  		assert(codeBits(codeTb)('d') === List(1,1,1))
  	}
  	
  	test("convert") {
  	  var codeTree = createCodeTree("hello".toList)
  	  var res = convert(codeTree)
  	  //println(res)
  	}
  	
  	test("mergeCodeTables") {
  	  var a = ('a', List(0,0,0)) :: Nil
  	  var b = ('b', List(0,0,1)) :: Nil
  	  assert(mergeCodeTables(a, b) === List(('a',List(0, 0, 0)), ('b',List(0, 0, 1))))
  	  
//  	 var c = ('c', List(0,1,1)) :: ('d', List(1,1,1)) :: Nil
//  	 var res = mergeCodeTables(a, c)
//  
//  	 println(res)
  	}
  	
  	test("quickEncode") {
  	  var codeTree = createCodeTree("hello".toList)
  	  var res = quickEncode(codeTree)("hello".toList)
  	  //println(res)
  	}
  	
  	test("finsh him") {
  	  val msg = "The Huffman encoding of this message should be three hundred and fifty-two bits long".toList
  	  val len = encode(createCodeTree(msg))(msg).size // should be 352
  	  println(len)
  	}
}
