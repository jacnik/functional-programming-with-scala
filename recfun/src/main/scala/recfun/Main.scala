package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def isBalanced(nb: Int, chars: List[Char]): Boolean = {
      if (nb < 0) false
      else if (chars.isEmpty) nb == 0
      else if (chars.head == '(') isBalanced(nb + 1, chars.tail)
      else if (chars.head == ')') isBalanced(nb - 1, chars.tail)
      else isBalanced(nb, chars.tail)
    }
    isBalanced(0, chars)
  }
  
  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
	def test(acc: Int, coins: List[Int]): Int = {
	  if(acc > money || coins.isEmpty) 0
	  else if (acc == money) 1
	  else test(acc + coins.head, coins) + test(acc, coins.tail)
	}
    
	if (coins.isEmpty) 0
	else test(0, coins)
  }
}
