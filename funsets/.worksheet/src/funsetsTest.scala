import funsets.FunSets._

object funsetsTest {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(90); 
  println("Welcome to the Scala worksheet");$skip(29); 
  
  val s = singletonSet(1);System.out.println("""s  : Int => Boolean = """ + $show(s ));$skip(37); 
  val s1 = union(s, singletonSet(2));System.out.println("""s1  : Int => Boolean = """ + $show(s1 ));$skip(37); 
  val p = union(s1, singletonSet(3));System.out.println("""p  : Int => Boolean = """ + $show(p ));$skip(17); 
  
  printSet(p);$skip(64); 
  
  def helper(x: Int): Boolean = {
  	println(x)
  	x < 3
  };System.out.println("""helper: (x: Int)Boolean""");$skip(23); val res$0 = 
  
  forall(p, helper);System.out.println("""res0: Boolean = """ + $show(res$0));$skip(19); val res$1 = 
	exists(p, helper);System.out.println("""res1: Boolean = """ + $show(res$1));$skip(35); 
 	val m = map(p, (x: Int) => x *x);System.out.println("""m  : Int => Boolean = """ + $show(m ));$skip(14); 
 	printSet(m)}
 
}
