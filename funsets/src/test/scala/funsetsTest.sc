import funsets.FunSets._

object funsetsTest {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val s = singletonSet(1)                         //> s  : Int => Boolean = <function1>
  val s1 = union(s, singletonSet(2))              //> s1  : Int => Boolean = <function1>
  val p = union(s1, singletonSet(3))              //> p  : Int => Boolean = <function1>
  
  printSet(p)                                     //> {1,2,3}
  
  def helper(x: Int): Boolean = {
  	println(x)
  	x < 3
  }                                               //> helper: (x: Int)Boolean
  
  forall(p, helper)                               //> 1
                                                  //| 2
                                                  //| 3
                                                  //| res0: Boolean = false
	exists(p, helper)                         //> 1
                                                  //| res1: Boolean = true
 	val m = map(p, (x: Int) => x *x)          //> m  : Int => Boolean = <function1>
 	printSet(m)                               //> {1,4,9}
 
}